package com.cxd.entity;

import com.cxd.ApplicationTests;
import com.cxd.repository.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author cxd
 * Date: 2018/08/01
 * Description:
 */
public class UserRepositpryTest extends ApplicationTests {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testFindOne(){
        User user = userRepository.findOne(2L);
        Assert.assertEquals("admin",user.getName());
    }
}
