package com.cxd.repository;

import com.cxd.entity.Subway;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author: cxd
 * @Date: 2018/08/05
 * @Description: 角色数据DAO
 */
public interface SubwayRepository extends CrudRepository<Subway, Long>{
    List<Subway> findAllByCityEnName(String cityEnName);
}
