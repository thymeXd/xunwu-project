package com.cxd.repository;

import com.cxd.entity.Role;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author: cxd
 * @Date: 2018/08/05
 * @Description: 角色数据DAO
 */
public interface RoleRepository extends CrudRepository<Role,Long> {

    List<Role> findRolesByUserId(Long userId);
}
