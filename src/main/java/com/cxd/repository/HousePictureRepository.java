package com.cxd.repository;

import com.cxd.entity.HousePicture;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author: cxd
 * @Date: 2018/08/05
 * @Description: 角色数据DAO
 */
public interface HousePictureRepository extends CrudRepository<HousePicture, Long> {
    List<HousePicture> findAllByHouseId(Long id);
}
